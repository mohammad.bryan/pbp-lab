import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Booking',
      theme: ThemeData(
        brightness: Brightness.dark,
        // primaryColor: Colors.lightBlue[800],
        fontFamily: "Georgia",
      ),
      home: HomePage(),
    );
  }
}


class HomePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Booking Guru"),
        backgroundColor: Colors.teal[800],
      ),
      body: 
      Center(
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 40),
          child: ListView(
            children:[ cardBuild(
              "Mohammad Bryan Mahdavikhia",
              "Pria",
              "Matematika",
              "https://assets.pikiran-rakyat.com/crop/0x0:0x0/x/photo/2020/11/24/722236635.jpg"
            ),
            cardBuild(
            "Emma Watson", 
            "Wanita", 
            "Bahasa Inggris", 
            "https://static.republika.co.id/uploads/images/inpicture_slide/emma-watson-bekerja-untuk-pbb-guna-mengampanyekan-women-s-heforshe-_160922081825-128.jpg"),
            cardBuild(
            "Raisa Andriana", 
            "Wanita", 
            "Bahasa Indonesia", 
            "https://asset.kompas.com/crops/gi6ACpnnR2UsrR6_VFl-V7gsP7s=/0x0:3500x2333/750x500/data/photo/2019/11/26/5ddce810e74fb.jpg"),
            ]
          )
        )
      )
    );
  }
  Card cardBuild(String nama, String jenis, String mapel, String imageLink){
    return Card(
      elevation: 20,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20)
      ),
      child: Column(
         children: [
          SizedBox(
            height: 200.0,
            child: Ink.image(
              image: NetworkImage(imageLink),
              fit: BoxFit.contain,
            ),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 5),
              alignment: Alignment.centerLeft,
              child: 
              Text(nama),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
              alignment: Alignment.centerLeft,
              child: 
              Text(jenis),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
              alignment: Alignment.centerLeft,
              child: 
              Text(mapel),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
              alignment: Alignment.centerRight,
              child: 
              FloatingActionButton.extended( 
                extendedPadding: EdgeInsetsDirectional.fromSTEB(20, 10, 20, 10), 
                onPressed: () {},  
                icon: Icon(Icons.save),  
                label: Text("Book"),
              ),
          ),
        ]
      ),
    );
  }
}
