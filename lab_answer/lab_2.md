1. Apakah perbedaan antara JSON dan XML?

 - JSON adalah standar terbuka yang ringan untuk pertukaran data. Ini telah dirancang untuk data yang dapat dibaca manusia. Bilangan, Boolean, string, null, array, objek, dll. Dapat direpresentasikan dengan menggunakan JSON. Sedangkan XML dapat dianggap sebagai versi yang disederhanakan dari Bahasa Generalized Markup Language. Ini adalah bahasa markup berbasis teks dan standar Konsorsium World Wide Web yang memungkinkan seseorang untuk membuat tag sendiri.
 - JSON menggunakan array sedangkan XML tidak
 - Format JSON adalah pertukaran data sedangkan XML adalah bahasa markup
 - Pengembang JSON ditentukan oleh Douglas Crockford sedangkan pengembang XML adalah Konsorsium World Wide Web
 - Tipe data dari JSON adalah Jenis data skalar, struktur data dapat diekspresikan menggunakan array dan objek sedangkan tipe data XML tergantung pada skema XML untuk menambahkan informasi jenis
 - Pengkodean dari JSON hanya mendukung pengkodean UTF-8 sedangkan XML mendukung penyandian yang berbeda

 JSON dan XML memiliki tujuan yang berbeda. XML hanyalah bahasa markup yang digunakan untuk menambahkan info tambahan ke teks biasa, sedangkan JSON adalah cara yang efisien untuk merepresentasikan data terstruktur dalam format yang dapat dibaca manusia.

2. Apakah perbedaan antara HTML dan XML?
XML dan HTML adalah bahasa markup yang ditentukan untuk tujuan berbeda dan memiliki beberapa perbedaan. HTML dirancang untuk memfasilitasi transfer dokumen berbasis web atau bagaiamana format tampilan dari data. Sedangkan XML lebih kepada struktur dan konteksnya. XML dan HTML saling melengkapi dan mempunyai fungsi yang berbeda.

HTML (HyperText Markup Language) digunakan untuk membuat halaman web dan aplikasi web. Ini adalah bahasa komputer yang digunakan untuk menerapkan tata letak dan konvensi pemformatan ke dokumen teks. Bahasa markup membuat teks lebih interaktif dan dinamis. Itu dapat mengubah teks menjadi gambar, tabel, tautan, dll

XML (eXtensible Markup Language) juga digunakan untuk membuat halaman web dan aplikasi web. Tetapi ini adalah bahasa dinamis yang digunakan untuk mengangkut data dan bukan untuk menampilkan data. Tujuan desain XML berfokus pada kesederhanaan, umum, dan kegunaan di Internet.

- XML adalah singkatan dari eXtensible Markup Language sedangkan HTML adalah singkatan dari Hypertext Markup Language.
- XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data.
- XML didorong konten sedangkan HTML didorong oleh format.
- XML itu Case Sensitive sedangkan XML Case Insensitive
- XML menyediakan dukungan namespaces sementara HTML tidak menyediakan dukungan namespaces.
- XML strict untuk tag penutup sedangkan HTML tidak strict.
- Tag XML dapat dikembangkan sedangkan HTML memiliki tag terbatas.
- Tag XML tidak ditentukan sebelumnya sedangkan HTML memiliki tag yang telah ditentukan sebelumnya.