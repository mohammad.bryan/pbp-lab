from django.db import models

# Create your models here.
class Note(models.Model):
    penerima = models.TextField()
    penulis = models.TextField()
    judul = models.TextField()
    pesan = models.TextField()
